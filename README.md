# Install

- Go to web folder by SSH.

- [Install composer](https://getcomposer.org/download/).

- Run commands 
```
git clone https://bitbucket.org/toXXic/ispconfig-letsencrypt.git ./
php composer.phar install
```

- Set DOCROOT to `web`. In ISPConfig add to `Apache Directives`
```
DocumentRoot "{DOCROOT}/web/"
```

# Update

```
cd toWebFolder
git pull
php composer.phar update
```

# Use

http://yourdomain/
