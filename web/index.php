<?php
/**
 * Created by PhpStorm.
 * User: toXXic
 * Date: 23/03/16
 * Time: 12:34
 */

define('ROOTDIR', __DIR__  . '/../');
define('SRCDIR', __DIR__  . '/../src/');

require ROOTDIR . 'vendor/autoload.php';

$ParseDown = new Parsedown();
$mainTemplate = file_get_contents(SRCDIR . 'main.html');

$domain = '';
$subDomain = '';
$addWww = FALSE;
$server = 'apache';
if (array_key_exists('domain', $_REQUEST)) {
    $domain = trim($_REQUEST['domain']);
}
if (array_key_exists('sub-domain', $_REQUEST)) {
    $subDomain = trim($_REQUEST['sub-domain']);
}
if (array_key_exists('addwww', $_REQUEST)) {
    $addWww = (int)($_REQUEST['addwww']);
}
if (array_key_exists('server', $_REQUEST)) {
    $tmp = strtolower(trim($_REQUEST['server']));
    if ($tmp === 'apache' || $tmp === 'nginx') {
        $server = $tmp;
    }
}

$shCmd = '';
if ($domain) {
    $subDomains = str_replace(',', "\n", $subDomain);
    if ($addWww && strpos($subDomains,'www.'.$domain) === FALSE) {
        $subDomains = 'www.'.$domain . "\n" . $subDomains;
        $subDomains = trim($subDomains);
    }
    $subDomains = explode("\n", $subDomains);
    $subDomains = array_map('trim', $subDomains);

    $dDomains = '';
    foreach ($subDomains as $value) {
        if ($value) {
            $dDomains .= ' -d ' . $value;
        }
    }

    $shTemplate = $server==='nginx'?'sh_nginx.md':'sh.md';
    $shCmd = file_get_contents(SRCDIR . $shTemplate);
    $shCmd = str_replace([
        '##domain##',
        '##domainSlash##',
        '##subdomains##',
        '##d-domains##',
    ], [
        $domain,
        str_replace('.','\.',$domain),
        $subDomains?', '.implode(', ',$subDomains):'',
        $dDomains,
    ], $shCmd);
    $shCmd = $ParseDown->text($shCmd);
}



$mainTemplate = str_replace([
    '##block##',
    '##domain##',
    '##subdomain##',
    '##addwwwchecked##',
    '##apacheselected##',
    '##nginxselected##',
], [
    $shCmd,
    $domain,
    $subDomain,
    $addWww?'checked':'',
    $server==='apache'?'selected':'',
    $server==='nginx'?'selected':'',
], $mainTemplate);

echo $mainTemplate;
