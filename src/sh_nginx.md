## Install LetsEncrypt for nginx ##

If already install goto [Make and enable certificate](#make-and-enable-certificate).

Log in as root, then
```
apt-get install git
cd ~
git clone https://github.com/letsencrypt/letsencrypt
openssl dhparam -out /var/dhparams.pem 2048
```

<a name="make-and-enable-certificate"></a>
## Make and enable certificate for nginx ##

We will generate keys for domains: __##domain####subdomains##__.

In ISPConfig go to `Websites -> ##domain## -> Domain` and check the `SSL` checkbox.

Then go to tab `SSL`. Enter values in the `State`, `Locality`, `Organisation`, 
`Organisation Unit`, `Country` fields and then at the bottom of the page under SSL Action select 
`Create Certificate` and click `Save`.

Then go to tab `Websites -> ##domain## -> Options`. To `nginx Directives` add
```
# redirect http to https
if ($scheme != "https") {
    rewrite ^ https://##domain##$request_uri? permanent;
}
# redirect from all domains & subdomains to main domain with https
if ($http_host !~ "^##domainSlash##$"){
    rewrite ^ https://##domain##$request_uri? permanent;
}
# for letsencrypt certificates
location ^~ /.well-known/acme-challenge/ {
    default_type 'text/plain';
    root /tmp/letsencrypt;
}
# ssl security params for A+ score by ssl labs
ssl_prefer_server_ciphers on;
ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
ssl_dhparam /var/dhparams.pem;
ssl_session_cache shared:TLS:2m;
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8;
add_header Strict-Transport-Security 'max-age=31536000; includeSubDomains';

##
# add you own nginx conf below
```

Wait `1 minute`.

Then go to console as `root` 
```
mkdir -p /tmp/letsencrypt
cd /root/letsencrypt
git pull
./letsencrypt-auto certonly \
  --server https://acme-v01.api.letsencrypt.org/directory \
  -a webroot --webroot-path=/tmp/letsencrypt --agree-tos \
  --rsa-key-size 4096 \
  -d ##domain####d-domains##
mv /var/www/##domain##/ssl/##domain##.crt /var/www/##domain##/ssl/##domain##.crt.old
mv /var/www/##domain##/ssl/##domain##.key /var/www/##domain##/ssl/##domain##.key.old
ln -s /etc/letsencrypt/live/##domain##/privkey.pem /var/www/##domain##/ssl/##domain##.key
ln -s /etc/letsencrypt/live/##domain##/fullchain.pem /var/www/##domain##/ssl/##domain##.crt
service nginx reload
```

## Auto update certificate ##

Certificate will work for 90 day or above. We need to update it several times in month.

Add to CRON as `root`
```
crontab -u root -e
```

this command:
```
30  1   10  1,3,5,7,9,11    *   mkdir -p /tmp/letsencrypt && cd /root/letsencrypt/ && git pull && ./letsencrypt-auto renew
```

## Check SSL ##

You can check your ssl configuration at [ssllabs.com](https://www.ssllabs.com/ssltest/analyze.html?d=##domain##).

<br><br>
