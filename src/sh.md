## Install LetsEncrypt ##

If already install goto [Make and enable certificate](#make-and-enable-certificate).

Log in as root, then
```
apt-get install git
cd ~
git clone https://github.com/letsencrypt/letsencrypt
cd letsencrypt
a2enmod proxy proxy_http
service apache2 restart
vi /etc/apache2/mods-enabled/proxy.conf
```

Add this to opened `proxy.conf`
```
<IfModule mod_proxy.c>
    ProxyPass "/.well-known/acme-challenge/" "http://127.0.0.1:9119/.well-known/acme-challenge/" retry=1
    ProxyPassReverse "/.well-known/acme-challenge/" "http://127.0.0.1:9119/.well-known/acme-challenge/"
    <Location "/.well-known/acme-challenge/">
    ProxyPreserveHost On
    Order allow,deny
    Allow from all
    Require all granted
    </Location>
</IfModule>
```

Then do
```
service apache2 restart
```

<a name="make-and-enable-certificate"></a>
## Make and enable certificate ##

We will generate keys for domains: __##domain####subdomains##__.

In ISPConfig go to `Websites -> ##domain## -> Domain` and check the `SSL` checkbox.
Then go to tab `SSL`. Enter values in the `State`, `Locality`, `Organisation`, 
`Organisation Unit`, `Country` fields and then at the bottom of the page under SSL Action select 
`Create Certificate` and click `Save`.
 
Then go to console as `root` 
```
cd /root/letsencrypt
git pull
./letsencrypt-auto --agree-tos --renew-by-default --standalone --standalone-supported-challenges http-01 --http-01-port 9119 --server https://acme-v01.api.letsencrypt.org/directory certonly -d ##domain####d-domains##
mv /var/www/##domain##/ssl/##domain##.crt /var/www/##domain##/ssl/##domain##.crt.old
mv /var/www/##domain##/ssl/##domain##.key /var/www/##domain##/ssl/##domain##.key.old
ln -s /etc/letsencrypt/live/##domain##/privkey.pem /var/www/##domain##/ssl/##domain##.key
ln -s /etc/letsencrypt/live/##domain##/cert.pem /var/www/##domain##/ssl/##domain##.crt
ln -s /etc/letsencrypt/live/##domain##/fullchain.pem /var/www/##domain##/ssl/##domain##.chain
service apache2 restart
```

In ISPConfig go to `Websites -> ##domain## -> Options`. To `Apache Directives` add
```
<IfModule mod_ssl.c>
    SSLCertificateChainFile /var/www/##domain##/ssl/##domain##.chain
</IfModule>
```

After 1 minute ssl will work.

## Auto update certificate ##

Certificate will work for 90 day or above. We need to update it several times in month.

Add to CRON as `root`
```
crontab -u root -e
```

this command:
```
30  1   10  1,3,5,7,9,11    *   cd /root/letsencrypt/ && git pull && ./letsencrypt-auto --agree-tos --renew-by-default --standalone --standalone-supported-challenges http-01 --http-01-port 9119 --server https://acme-v01.api.letsencrypt.org/directory certonly -d ##domain####d-domains##
```

## Apache redirect to HTTPS ##

If you need add redirect from __HTTP__ to __HTTPS__ and __*.##domain##__ to __##domain##__ add this to `.htaccess`
```
Options +FollowSymLinks
RewriteEngine On
RewriteCond %{HTTPS} off [OR]
RewriteCond %{HTTP_HOST} !^##domainSlash##$ [NC]
RewriteRule ^(.*)$ https://##domain##/$1 [R=301,L]
```

## Check SSL ##

You can check your ssl configuration at [ssllabs.com](https://www.ssllabs.com/ssltest/analyze.html?d=##domain##).

<br><br>
